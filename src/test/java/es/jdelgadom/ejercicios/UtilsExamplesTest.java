package es.jdelgadom.ejercicios;

import static org.junit.Assert.*;

import org.junit.Test;

import es.jdelgadom.model.Person;


public class UtilsExamplesTest {

  @Test
  public void testExistLastName() {
    UtilsExamples utilExample = new UtilsExamples();
    //1. Create the person to the first UnitCase Person = null
    Person personUnitCase1 = null;
    assertTrue("No esta devolviendo FALSE: existLastName",utilExample.existLastName(personUnitCase1)==false);
    //2. Create the person to the second UnitCase Person with LastName
    Person personUnitCase2 = new Person();
    personUnitCase2.setApellido1("Smith");
    assertTrue("No esta devolviendo TRUE: existLastName",utilExample.existLastName(personUnitCase2)==true);
    //3. Create the person to the third UnitCase Person without LastName
    Person personUnitCase3 = new Person();
    personUnitCase3.setApellido1("");
    assertTrue("No esta devolviendo FALSE: existLastName",utilExample.existLastName(personUnitCase3)==false);
  }
  
  @Test
  public void testNewSalary() {
    UtilsExamples utilExample = new UtilsExamples();
    Double errorDouble = new Double("-1.0");
    //1. Create the person to the first UnitCase Person = null
    Person personUnitCase1 = null;
    assertTrue("Debe devolver -1",utilExample.calculateSalary(personUnitCase1).equals(errorDouble));
    //2. Create the person to the second UnitCase Person with salary 100
    Person personUnitCase2 = new Person();
    personUnitCase2.setSueldoTrabajador(new Double("100.0"));
    Double resultCase2 = new Double("110.00000000000001");
    assertTrue("Debe devolver -1",utilExample.calculateSalary(personUnitCase2).equals(resultCase2));
    //3. Create the person to the first UnitCase Person with salary negative
    Person personUnitCase3 = new Person();
    personUnitCase3.setSueldoTrabajador(new Double("-2"));
    assertTrue("Debe devolver -1",utilExample.calculateSalary(personUnitCase3).equals(errorDouble));
    
  }
  
  @Test
  public void testMyNameIs() {
    UtilsExamples utilExample = new UtilsExamples();
    Person personUnitCase = new Person();
    //Person without any properties
    String resultTestOne = "No tiene nombre esta persona";
    assertTrue("Debe de devolver el mensaje correcto : "+utilExample.myNameIs(personUnitCase),utilExample.myNameIs(personUnitCase).equals(resultTestOne));
    //Person with name
    String resultTestTwo = "No tiene apellido1 esta persona";
    personUnitCase.setNombre("Jose");
    assertTrue("Debe de devolver el mensaje correcto: "+utilExample.myNameIs(personUnitCase),utilExample.myNameIs(personUnitCase).equals(resultTestTwo));
    //Person with name + lastname
    String resultTestThree = "No tiene puesto de trabajo esta persona";
    personUnitCase.setApellido1("Delgado");
    assertTrue("Debe de devolver el mensaje correcto: "+utilExample.myNameIs(personUnitCase),utilExample.myNameIs(personUnitCase).equals(resultTestThree));
    //Person with name + lastname
    String resultTestFour = "Soy Jose Delgado null y mi puesto es:Programador";
    personUnitCase.setPuestoTrabajo("Programador");
    assertTrue("Debe de devolver el mensaje correcto: "+utilExample.myNameIs(personUnitCase),utilExample.myNameIs(personUnitCase).equals(resultTestFour));
  }

}
