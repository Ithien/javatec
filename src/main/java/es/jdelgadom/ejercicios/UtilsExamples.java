package es.jdelgadom.ejercicios;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.jdelgadom.model.Person;

public class UtilsExamples {
  
  public static Logger logger = LoggerFactory.getLogger(UtilsExamples.class); 
  
  /**
   * 
   * @param person to test the LastName
   * @return false if the LastName is null or empty
   *         true  if the LastName is not null or not empty
   */
  public Boolean existLastName(Person person){
    try {
      logger.info("Entramos en UtilsExamples.existLastName");
      Boolean result = false;
      if (StringUtils.isNotEmpty(person.getApellido1())){
        result = true;
      }
      return result;
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.existLastName", e);
      return false;
    } finally {
      logger.info("Salimos de UtilsExamples.existLastName");
    }
  }
  
  /**
   * 
   * @param person
   * @return -1 error
   *         new salary is correct - with a 10% more
   */
  public Double calculateSalary(Person person){
    try {
      logger.info("Entramos en UtilsExamples.calculateSalary");
      Double newSalary = new Double("-1");
      Double plusSalary = new Double("1.1");
      if (person != null && person.getSueldoTrabajador() > 0){
        newSalary = person.getSueldoTrabajador() * plusSalary;
      }
      return newSalary;
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.calculateSalary", e);
      return new Double("-1");
    } finally {
      logger.info("Salimos de UtilsExamples.calculateSalary");
    }
  }

  /**
   * 
   * @param person
   * @return String with error or with the message with the name.
   */
  public String myNameIs(Person person){
    try {
      logger.info("Entramos en UtilsExamples.myNameIs");
      StringBuilder messageNameBuilder = new StringBuilder();
      
      if (StringUtils.isEmpty(person.getNombre())){
        messageNameBuilder.append("No tiene nombre esta persona");
      }else if (StringUtils.isEmpty(person.getApellido1())){
        messageNameBuilder.append("No tiene apellido1 esta persona");
      }else if (StringUtils.isEmpty(person.getPuestoTrabajo())){
        messageNameBuilder.append("No tiene puesto de trabajo esta persona");
      }else{
        messageNameBuilder.append("Soy ");
        messageNameBuilder.append(person.getNombre());
        messageNameBuilder.append(" ");
        messageNameBuilder.append(person.getApellido1());
        messageNameBuilder.append(" ");
        messageNameBuilder.append(person.getApellido2());
        messageNameBuilder.append(" y mi puesto es:");
        messageNameBuilder.append(person.getPuestoTrabajo());
      }
      
      return messageNameBuilder.toString();
      
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.myNameIs", e);
      return "Error no controlado.";
    } finally {
      logger.info("Salimos de UtilsExamples.myNameIs");
    }
  }
  
  /**
   * Test the name property is Jose
   * @param person
   * @return
   */
  public Boolean testNamePerson(Person person){
    try {
      logger.info("Entramos en UtilsExamples.testNamePerson");
      Boolean resultado = false;
      if("Jose".equals(person.getNombre())){
        resultado = true;
      }
      return resultado;
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.testNamePerson", e);
      return false;
    } finally {
      logger.info("Salimos de UtilsExamples.testNamePerson");
    }
  }
  
  public List<Person> createListPerson(){
    try {
      logger.info("Entramos en UtilsExamples.createListPerson");
      Person person1 = new Person("Ataulfo", "Ramirez", "Tecnico Campo", 1, 100.0);
      Person person2 = new Person("Garcilaso", "Ramirez", "Tecnico Campo", 2, 200.0);
      Person person3 = new Person("Camilo", "Sexto", "Animador", 3, 400.0);
      Person person4 = new Person("Clark", "Kent", "SuperHero", 4, 10000.0);
      Person person5 = new Person("Bob", "Esponja", "Gerente", 5, 25000.0);
      
      //1- Se crea la lista y se añaden todas las personas.
      List<Person> listPerson = new ArrayList<>();
      listPerson.add(person1);
      listPerson.add(person2);
      listPerson.add(person3);
      listPerson.add(person4);
      listPerson.add(person5);
      
      return listPerson;
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.createListPerson", e);
      return new ArrayList<>();
    } finally {
      logger.info("Salimos de UtilsExamples.createListPerson");
    }
  }
  
  /**
   * 
   * @param listPerson
   * @return true - if in the list of person have a person with name = Jose
   *         false - other case
   */
  public Boolean oneJoseOnList(List<Person> listPerson){
    try {
      logger.info("Entramos en UtilsExamples.oneJoseOnList");
      
      for (Person person : listPerson) {
        if (testNamePerson(person)){
          return true;
        }
      }
      
      return false;
      
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.oneJoseOnList", e);
      return false;
    } finally {
      logger.info("Salimos de UtilsExamples.oneJoseOnList");
    }
  }
  
  public Boolean oneJoseOnListOptimize(List<Person> listPerson){
    try {
      logger.info("Entramos en UtilsExamples.oneJoseOnListOptimize");
      EqualPredicate namePredicate = new EqualPredicate("Jose");
      BeanPredicate beanPredicate = new BeanPredicate("nombre", namePredicate);
      return CollectionUtils.exists(listPerson, beanPredicate);
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.oneJoseOnListOptimize", e);
      return false;
    } finally {
      logger.info("Salimos de UtilsExamples.oneJoseOnListOptimize");
    }
  }
  
  public String concatChars(String palabra1, String palabra2){
    try {
      logger.info("Entramos en UtilsExamples.concatChars");
      return (palabra1.substring(0,1) + palabra2.substring(palabra2.length()-1));
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.concatChars", e);
      return "errorrrrr";
    } finally {
      logger.info("Salimos de UtilsExamples.concatChars");
    }
  }
  
  public String concatNTimes(String palabra1, Integer nTimes){
    try {
      logger.info("Entramos en UtilsExamples.concatNTimes");
      String resultado = "";
      for (int i = 0; i < nTimes; i++) {
        resultado = resultado + palabra1;
      }
      return resultado;
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.concatNTimes", e);
      return "error: :(";
    } finally {
      logger.info("Salimos de UtilsExamples.concatNTimes");
    }
    
  }
  
  /**
   * To test the code in a minut
   * @param args
   */
  public static void main(String[] args) {
    UtilsExamples utilsExamples = new UtilsExamples();
    
    
    List<Person> listPerson = utilsExamples.createListPerson();
    
    System.out.println("concatNTimes: " + utilsExamples.concatNTimes("Hola", 3));
    
    System.out.println("concatenamos palabras: " + utilsExamples.concatChars("yo", "java"));
    
    System.out.println("El tamanyo de la lista es: " + listPerson.size());
    
    System.out.println("En la lista hay algun jose? " + utilsExamples.oneJoseOnList(listPerson));
    
    System.out.println("En la lista hay algun jose? funcion optimize: " + utilsExamples.oneJoseOnListOptimize(listPerson));
  }
  
}
