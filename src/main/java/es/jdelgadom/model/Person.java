package es.jdelgadom.model;

import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.jdelgadom.ejercicios.UtilsExamples;

public class Person {
  
  public static Logger logger = LoggerFactory.getLogger(UtilsExamples.class); 
  
  private String nombre;
  private String apellido1;
  private String apellido2;
  private String puestoTrabajo;
  private Integer idTrabajador;
  private Double sueldoTrabajador;
  private Boolean estaDeBaja;
  private GregorianCalendar fechaDeContratacion;
  
  public String getNombre() {
    return nombre;
  }
  
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  public String getApellido1() {
    return apellido1;
  }
  
  public void setApellido1(String apellido1) {
    this.apellido1 = apellido1;
  }
  
  public String getApellido2() {
    return apellido2;
  }
  
  public void setApellido2(String apellido2) {
    this.apellido2 = apellido2;
  }
  
  public String getPuestoTrabajo() {
    return puestoTrabajo;
  }
  
  public void setPuestoTrabajo(String puestoTrabajo) {
    this.puestoTrabajo = puestoTrabajo;
  }
  
  public Integer getIdTrabajador() {
    return idTrabajador;
  }
  
  public void setIdTrabajador(Integer idTrabajador) {
    this.idTrabajador = idTrabajador;
  }
  
  public Double getSueldoTrabajador() {
    return sueldoTrabajador;
  }
  
  public void setSueldoTrabajador(Double sueldoTrabajador) {
    this.sueldoTrabajador = sueldoTrabajador;
  }
  
  public Boolean getEstaDeBaja() {
    return estaDeBaja;
  }
  
  public void setEstaDeBaja(Boolean estaDeBaja) {
    this.estaDeBaja = estaDeBaja;
  }
  
  public GregorianCalendar getFechaDeContratacion() {
    return fechaDeContratacion;
  }
  
  public void setFechaDeContratacion(GregorianCalendar fechaDeContratacion) {
    this.fechaDeContratacion = fechaDeContratacion;
  }

  public Person() {
  }

  public Person(String nombre, String apellido1, String puestoTrabajo, Integer idTrabajador, Double sueldoTrabajador) {
    this.nombre = nombre;
    this.apellido1 = apellido1;
    this.puestoTrabajo = puestoTrabajo;
    this.idTrabajador = idTrabajador;
    this.sueldoTrabajador = sueldoTrabajador;
  }

  public Person(String nombre, String apellido1, String apellido2, String puestoTrabajo, Integer idTrabajador, Double sueldoTrabajador, Boolean estaDeBaja, GregorianCalendar fechaDeContratacion) {
    this.nombre = nombre;
    this.apellido1 = apellido1;
    this.apellido2 = apellido2;
    this.puestoTrabajo = puestoTrabajo;
    this.idTrabajador = idTrabajador;
    this.sueldoTrabajador = sueldoTrabajador;
    this.estaDeBaja = estaDeBaja;
    this.fechaDeContratacion = fechaDeContratacion;
  }
  
  
  public Double calculateIrpf(Double sueldo){
    try {
      logger.info("Entramos en UtilsExamples.calculateIrpf");
      Double newSalary = new Double("-1");
      //Se retiene un 15% por lo que se anyade un 15% mas al sueldo.
      Double plusSalary = new Double("1.15");
      if (sueldo != null && sueldo > 0){
        newSalary = sueldo * plusSalary;
      }
      return newSalary;
    } catch (Exception e) {
      logger.error("Error en UtilsExamples.calculateIrpf", e);
      return new Double("-1");
    } finally {
      logger.info("Salimos de UtilsExamples.calculateIrpf");
    }
  }
  
}
